#include <iostream>
#include "tickets.h"
#include "testdatareader.h"
#include <chrono>
#include <functional>

std::ostream& addSpace(std::ostream& stream)
{
    stream << std::string(5, ' ');
    return stream;
}

bool algorithmTest(int testCaseNum, const std::function<long long unsigned(int)>& algorithm)
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    const auto result = algorithm(testCaseNum + 1);
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    const auto [input, expected] = otus_algo::readTestData(testCaseNum);
    const bool accepted = (expected == result);

    std::cout << otus_algo::testName(testCaseNum) << " : " << (accepted ? "ACCEPTED" : "FAILED") << std::endl
              << addSpace << "input: " << input << std::endl
              << addSpace << "expected: " << expected << std::endl
              << addSpace << "result: " << result << std::endl
              << addSpace << "time: " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << " ms" << std::endl
              << std::endl;

    return accepted;
}

void bruteForseTest(int maxTestCase)
{
    std::cout << "BruteForce test" << std::endl;
    std::cout << "Test cases: [0;" << maxTestCase <<"]" << std::endl;
    for(int i = 0; i <= maxTestCase; ++i)
    {
        if(!algorithmTest(i, otus_algo::getLuckyCount_BruteForce))
        {
            return;
        }
    }
}

void dynamicProgrammingTest(int maxTestCase)
{
    std::cout << "DynamicProgramming test" << std::endl;
    std::cout << "Test cases: [0;" << maxTestCase <<"]" << std::endl;
    for(int i = 0; i <= maxTestCase; ++i)
    {
        if(!algorithmTest(i, otus_algo::getLuckyCount_DynamicProgramming))
        {
            return;
        }
    }
}

int main(int, char**)
{
    bruteForseTest(4);
    dynamicProgrammingTest(9);

    return 0;
}
