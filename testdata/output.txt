BruteForce test
Test cases: [0;4]
test.0 : ACCEPTED
     input: 1
     expected: 10
     result: 10
     time: 2 ms

test.1 : ACCEPTED
     input: 2
     expected: 670
     result: 670
     time: 24 ms

test.2 : ACCEPTED
     input: 3
     expected: 55252
     result: 55252
     time: 2376 ms

test.3 : ACCEPTED
     input: 4
     expected: 4816030
     result: 4816030
     time: 237894 ms

test.4 : ACCEPTED
     input: 5
     expected: 432457640
     result: 432457640
     time: 23777727 ms

DynamicProgramming test
Test cases: [0;9]
test.0 : ACCEPTED
     input: 1
     expected: 10
     result: 10
     time: 2 ms

test.1 : ACCEPTED
     input: 2
     expected: 670
     result: 670
     time: 3 ms

test.2 : ACCEPTED
     input: 3
     expected: 55252
     result: 55252
     time: 4 ms

test.3 : ACCEPTED
     input: 4
     expected: 4816030
     result: 4816030
     time: 7 ms

test.4 : ACCEPTED
     input: 5
     expected: 432457640
     result: 432457640
     time: 9 ms

test.5 : ACCEPTED
     input: 6
     expected: 39581170420
     result: 39581170420
     time: 13 ms

test.6 : ACCEPTED
     input: 7
     expected: 3671331273480
     result: 3671331273480
     time: 18 ms

test.7 : ACCEPTED
     input: 8
     expected: 343900019857310
     result: 343900019857310
     time: 21 ms

test.8 : ACCEPTED
     input: 9
     expected: 32458256583753952
     result: 32458256583753952
     time: 26 ms

test.9 : ACCEPTED
     input: 10
     expected: 3081918923741896840
     result: 3081918923741896840
     time: 31 ms