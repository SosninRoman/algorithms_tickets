#include "testdatareader.h"
#include <filesystem>
#include <fstream>
#include <iostream>

namespace
{

long long unsigned readTestValue(std::filesystem::path path)
{
    std::fstream in(path);
    const std::size_t& size = std::filesystem::file_size(path);
    std::string content(size, '\0');
    in.read(content.data(), size);
    return std::stoll(content);
}

}

namespace otus_algo
{

std::string testName(int N)
{
    return std::string("test.").append(std::to_string(N));
}

std::pair<long long unsigned, long long unsigned> readTestData(int N)
{
    const auto testNameStr = testName(N);
    return {readTestValue(std::filesystem::path(TEST_DATA_PATH) / (testNameStr + ".in")),
            readTestValue(std::filesystem::path(TEST_DATA_PATH) / (testNameStr + ".out"))};
}



}
