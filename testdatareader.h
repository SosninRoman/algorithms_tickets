#ifndef TESTDATAREADER_H
#define TESTDATAREADER_H

#include <tuple>
#include <string>

namespace otus_algo
{

//Сформировать имя теста оп его номеру
std::string testName(int N);

//Прочитать данные теста с определенным номером
//first - значение теста
//second - значение результата
std::pair<long long unsigned, long long unsigned> readTestData(int N);

}

#endif
