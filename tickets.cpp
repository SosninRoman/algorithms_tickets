#include "tickets.h"
#include <vector>
#include <numeric>

namespace
{

int getLuckyCountImpl(int N, long long unsigned sumL, long long unsigned sumR)
{
    if(!N)
    {
        return (sumL == sumR);
    }

    int res = 0;
    for(int l = 0; l < 10; ++l)
    {
        for(int r = 0; r < 10; ++r)
        {
            res += getLuckyCountImpl(N-1, sumL + l, sumR + r);
        }
    }
    return res;
}

std::vector<long long unsigned> process(const std::vector<long long unsigned>& sums)
{
    std::vector<long long unsigned> result(sums.size() + 9, 0);
    for(int row = 0; row < 10; ++row)
    {
        for(std::vector<long long unsigned>::size_type sumValuePos = 0; sumValuePos < sums.size(); ++sumValuePos)
        {
            result[sumValuePos + row] += sums[sumValuePos];
        }
    }
    return result;
}

}

namespace otus_algo
{

unsigned long long getLuckyCount_BruteForce(int n)
{
    return getLuckyCountImpl(n, 0, 0);
}


long long unsigned getLuckyCount_DynamicProgramming(int n)
{
    std::vector<long long unsigned> sums(10, 1);
    while(n > 1)
    {
        sums = process(sums);
        --n;
    }
    return std::accumulate(sums.cbegin(), sums.cend(), (long long unsigned)0, [](long long unsigned val, long long unsigned nextVal){return val + nextVal*nextVal;});
}

}
