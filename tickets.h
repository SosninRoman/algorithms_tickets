#ifndef TICKETS_H
#define TICKETS_H

namespace otus_algo
{

//Вычисление количества счастливых билетов перебором всех значений
long long unsigned getLuckyCount_BruteForce(int n);

//Вычисление количества счастливых билетов методом динамического программирования
unsigned long long getLuckyCount_DynamicProgramming(int n);

}

#endif
